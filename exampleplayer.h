#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {
private:
	Board* b;           //an instance of the board class
	Side s;             //side we are on (either WHITE or BLACK)
	Side opp_s;         //the opponents side (opposite of our s)
	Move* m;            //current move

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
