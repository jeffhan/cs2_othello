//this file has been edited by jeff han
#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {

     this->b = new Board();           //create a new board
     this->s = side;                  //set the side we are one
     if (this->s == WHITE){           //set opponenets side
        this->opp_s = BLACK;
     }
     else {
        this->opp_s = WHITE;
     }
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
    delete b;                        //may need to do more than just this
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * Currently, we do the last possible move checked.  
     */
     if(opponentsMove != NULL) {                  //add opponents move to our board if it is not NULL
        b->doMove(opponentsMove, this->opp_s);    //since we have to update our board ourselves     
     }

     if (!(b->hasMoves(this->s))){                //check that we have valid moves
        cerr << "no moves!" << endl;
        return NULL;                              //no valid moves present, returns null
     }

     for (int i = 0; i < 8; i ++) {               //loop through rows
         for (int j = 0; j < 8; j ++) {           //and columns
             Move* mt = new Move(i, j);           //create a new move 
             if (b->checkMove(mt, this->s)) {     //this move is possible
                 this->m = mt;                    //set it as the current move
             }
             //    will get mem leaks because of all the new Move instances, so figure out a way to delete
         }
     }
     b->doMove(m, s);                             //add the move to the board
     
     return this->m;                              //return the move.
}
    
